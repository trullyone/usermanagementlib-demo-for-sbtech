﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBTech.UserManagement.Model;

namespace SBTech.UserManagementUnitTests.Model
{
    class LambdaUserDataStore : IUserDataStore
    {
        private Action<User> persistData;
        
        public LambdaUserDataStore(Action<User> persistData) 
        {
            if (persistData == null) throw new ArgumentNullException("persistData");
            this.persistData = persistData;
        }

        public void PersistData(User user)
        {
            this.persistData(user);
        }
    }
}
