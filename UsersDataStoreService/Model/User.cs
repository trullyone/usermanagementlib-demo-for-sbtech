﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTech.UserManagement.Model
{
    public class User : IUser
    {
        public String FirstName { set; get; }
        public String LastName { set; get; }

        public User(String firstName, String lastName) 
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}
