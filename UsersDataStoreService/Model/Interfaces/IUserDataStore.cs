﻿using System;
namespace SBTech.UserManagement.Model
{
    public interface IUserDataStore
    {
        void PersistData(User user);
    }
}
