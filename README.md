# README #

### Launching the unit tests ###
1. Build the project.
2. Launch the result file from UsersDataStoreServiceUnitTests project in NUnit tests-runner.

If you like only to run the unit tests, you can skip the following part. 

### MSSQL Database Setup ###

1. Open MSSQL Server Management Studio.
2. Run UsersEFModel.edmx.sql DDL script located in the UserDataStore project.
3. Update UserDataStoreClient's app.config.


