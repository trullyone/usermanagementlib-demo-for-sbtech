﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTech.UserManagement.Model
{
    [Serializable]
    public class UserManagementException : Exception
    {
        public UserManagementException() { }
        public UserManagementException(string message) : base(message) { }
        public UserManagementException(string message, Exception inner) : base(message, inner) { }
        protected UserManagementException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
