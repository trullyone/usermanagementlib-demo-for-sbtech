﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTech.UserManagement.Model
{
    public interface IUser
    {
        String FirstName { set; get; }
        String LastName { set; get; }
    }
}
