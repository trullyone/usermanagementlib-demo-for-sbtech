﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTech.UserManagement.Model
{
    public class UserManagement
    {
        private IUserDataStore dataStore;

        public UserManagement(IUserDataStore dataStore) 
        {
            if (dataStore == null) throw new ArgumentNullException("dataStore");
            this.dataStore = dataStore;
        }
        
        public void SaveUser(User user, Action<string> log = null) 
        {
            if (user == null) throw new ArgumentNullException("user");
            try
            {
                this.dataStore.PersistData(user);
                if (log != null) 
                {
                    log("Data stored successfully.");
                }
            }
            catch (Exception ex)
            {
                if (log != null)
                {
                    log("An error has occured: " + ex.StackTrace);
                }
                throw new UserManagementException("An error has occured.", ex);
            }
        }
    }
}
