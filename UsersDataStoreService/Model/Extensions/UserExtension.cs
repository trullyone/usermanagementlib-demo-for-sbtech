﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTech.UserManagement.Model.Extensions
{
    public static class UserExtensions
    {
        public static T WithDataFrom<T>(this T toUser, IUser fromUser) where T : IUser
        {
            toUser.FirstName = fromUser.FirstName;
            toUser.LastName = fromUser.LastName;
            return toUser;
        }
    }
}
