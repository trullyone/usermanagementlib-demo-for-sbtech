﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SBTech.UserManagement.Model;

namespace SBTech.UserManagementUnitTests.Model
{
    [TestFixture]
    class UsersPersistenceUnitTests
    {
        [Test]
        public void OnNonNullUserObjectArgument_CallingUserManagementSaveUser_CallsDataStorePersistUser() 
        {
            var persistUserCalled = false;
            var userDataModel = new SBTech.UserManagement.Model.UserManagement(new LambdaUserDataStore(u => persistUserCalled = true));
            userDataModel.SaveUser(new User("Niki", "Kunev"));
            Assert.That(persistUserCalled, Is.True);
        }

        [Test]
        public void OnNullUserObjectArgument_CallingUserManagementSaveUser_ThrowsArgumentNullException()
        {
            var userDataModel = new SBTech.UserManagement.Model.UserManagement(new LambdaUserDataStore(u => { }));
            Assert.That(() => userDataModel.SaveUser(null), Throws.ArgumentNullException);
        }

        [Test]
        public void OnAnyException_CallingUserManagementSaveUser_ThrowsUserManagementExceptionWithCorrectInnerException()
        {
            const string innerExceptionName = "SBTInnerExceptionName123";
            var userDataModel = new SBTech.UserManagement.Model.UserManagement(new LambdaUserDataStore(u => { throw new Exception(innerExceptionName); }));
            Assert.That(() => userDataModel.SaveUser(new User("Niki", "Kunev")), Throws.InstanceOf<UserManagementException>().And.InnerException.Message.EqualTo(innerExceptionName));
        }
    }
}
