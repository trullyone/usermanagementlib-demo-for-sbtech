﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBTech.UserManagement.Model;

namespace UserManagementConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            bool quit;
            do
            {
                Console.WriteLine("Enter First Name:");
                var firstName = Console.ReadLine();
                Console.WriteLine("Enter First Name:");
                var lastName = Console.ReadLine();
                var userDataModel = new UserManagement(new EFUserDataStore());
                userDataModel.SaveUser(new User(firstName, lastName), log => Console.WriteLine(log));
                Console.WriteLine("Add another user? y/N");
                quit = Console.ReadLine().ToLower() != "y";
            } 
            while (!quit);
        }
    }
}
