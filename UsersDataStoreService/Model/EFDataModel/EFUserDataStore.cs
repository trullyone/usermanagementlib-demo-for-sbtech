﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SBTech.UserManagement.Model.EFDataModel;
using SBTech.UserManagement.Model.Extensions;

namespace SBTech.UserManagement.Model
{
    public class EFUserDataStore : IUserDataStore
    {

        public void PersistData(User user)
        {
            using (var model = new UsersEFModel()) 
            {
                var efuser = new EFUser().WithDataFrom(user);
                model.Users.Add(efuser);
                model.SaveChanges();
            }
        }
    }
}
